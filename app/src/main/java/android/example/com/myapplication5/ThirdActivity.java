package android.example.com.myapplication5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        webView = findViewById(R.id.webView01);

        WebSettings webSettings = webView.getSettings();
        webView.getSettings().setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(false);


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        webView.loadUrl("https://search.naver.com/search.naver?sm=tab_hty.top&where=nexearch&query=%EC%A0%84%EC%97%AD%EC%9D%BC%EA%B3%84%EC%82%B0%EA%B8%B0&oquery=%EC%A0%84%EC%97%AD%EC%9D%BC%EA%BC%90%EC%82%B0%EA%B8%B0&tqi=T9asnlpVuEwssbXT4vGssssstgV-349880");
    }
}
